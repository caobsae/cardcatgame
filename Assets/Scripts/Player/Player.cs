using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int hitsCanTake = 1;
    private int hitsLeft;

    private void Start()
    {
        hitsLeft = hitsCanTake;
    }

    public void TakeHit()
    {
        // Handle the player getting hit
        --hitsLeft; // For example, reduce health by 10
        if (hitsLeft <= 0)
        {
            // Handle player's death or game over
            Debug.Log("Has muerto");
        }
    }
}
