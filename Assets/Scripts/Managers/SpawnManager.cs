using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    public List<Transform> spawnPoints = new List<Transform>();
    public Transform playerTransform; // Reference to the player's transformr
    public GameObject enemyPrefab;
    private int totalEnemiesToSpawn = 1;
    private int enemiesSpawned = 0;

    private void Start()
    {
        // Initialize your spawner, set up the spawn points, etc.
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            StartNewRound();
        }
    }

    private void StartNewRound()
    {
        totalEnemiesToSpawn += 1; // Increase the number of enemies to spawn each round
        enemiesSpawned = 0;
        StartCoroutine(SpawnEnemies());
    }

    private IEnumerator SpawnEnemies()
    {
        while (enemiesSpawned < totalEnemiesToSpawn)
        {
            Transform spawnPoint = GetRandomSpawnPoint();
            GameObject newEnemy = Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);

            EnemyFollow enemyController = newEnemy.GetComponent<EnemyFollow>();
            if (enemyController != null)
            {
                enemyController.target = playerTransform;
            }
            GameManager.Instance.AddActiveEnemy(newEnemy.GetComponent<EnemyFollow>());
            newEnemy.transform.SetParent(this.gameObject.transform); // Set the spawned enemy as a child of the spawn point
            enemiesSpawned++;

            yield return new WaitForSeconds(1f); // Wait for a short time before spawning the next enemy
        }
    }

    private Transform GetRandomSpawnPoint()
    {
        int randomIndex = Random.Range(0, spawnPoints.Count);
        return spawnPoints[randomIndex];
    }
}
