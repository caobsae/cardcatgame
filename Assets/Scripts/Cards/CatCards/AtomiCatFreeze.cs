using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomiCatFreeze : Card
{
    private void Awake()
    {
        munition = 1;
        cooldown = 130;
        Id = 3;
        effectType = CardEffectType.OneEffect;
    }

    public override void Effect()
    {
        if (CanUse())
        {
            cooldownTimer = cooldown;
            munition--;

            ApplyFreezeEffect();
        }
    }

    private void ApplyFreezeEffect()
    {
        EnemyFollow[] enemies = FindObjectsOfType<EnemyFollow>();

        foreach (EnemyFollow enemy in enemies)
        {
            if (enemy.IsStunned())
            {
                enemy.Die();
            }
            else
            {
                enemy.ApplyStun(3f);
            }
        }
    }
}
