using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    public Transform target;           // Reference to the player's transform
    public float moveSpeed = 3f;       // Speed at which the enemy moves towards the player
    public int maxHealth = 20;
    private int currentHealth;
    private bool hasTakenOneEffectDamage = false; // Track if damage was dealt by OneEffect card
    private bool isStunned = false;
    private float stunTimer = 0f;

    private List<Card> oneEffectCardsColliding = new List<Card>(); // List of OneEffect cards colliding


    private void Start()
    {
        currentHealth = maxHealth;
    }


    private void Update()
    {
        if (isStunned)
        {
            stunTimer -= Time.deltaTime;
            if (stunTimer <= 0f)
            {
                isStunned = false;
            }
        }
        else
        {
            Vector3 directionToPlayer = target.position - transform.position;
            Vector3 moveDirection = directionToPlayer.normalized;

            transform.Translate(moveDirection * moveSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Enemigo tocado");
        if (other.CompareTag("Cat"))
        {
            Debug.Log("Me toc� una carta");
            Card card = other.GetComponent<Card>();
            if (card != null)
            {

                HandleCardCollision(card);
            }
        }
        else if (other.CompareTag("Player"))
        {
            Player player = other.GetComponent<Player>();
            if (player != null)
            {
                player.TakeHit();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Cat"))
        {
            Card card = other.GetComponent<Card>();

            if (card.effectType == CardEffectType.OneEffect)
            {
                if (oneEffectCardsColliding.Contains(card))
                {
                    oneEffectCardsColliding.Remove(card); // Remove from the list
                }
            }
        }
    }

    private void HandleCardCollision(Card card)
    {
        switch (card.effectType)
        {
            case CardEffectType.DPS:
                StartCoroutine(DoDPS(card.damage));
                break;
            case CardEffectType.OneHit:
                TakeHit(card.damage);
                card.gameObject.SetActive(false); // Hide the card after one hit
                break;
            case CardEffectType.InstaKill:
                Destroy(gameObject);
                break;
            case CardEffectType.OneEffect:
                if (!oneEffectCardsColliding.Contains(card))
                {
                    oneEffectCardsColliding.Add(card); // Add to the list
                    TakeHit(card.damage);
                }
                break;
        }
        Debug.Log($"Vida raton: {currentHealth}");
    }

    private IEnumerator DoDPS(float damage)
    {
        while (currentHealth > 0)
        {
            TakeHit(damage);
            yield return new WaitForSeconds(1f);
        }
    }

    private void TakeHit(float damage)
    {
        currentHealth -= (int)damage;
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void Die()
    {
        GameManager.Instance.RemoveActiveEnemy(this);
        Destroy(gameObject);
    }

    private void HandlePlayerCollision()
    {
        // Replace this with your desired behavior when the enemy touches the player
        Debug.Log("Enemy touched the player!");
        // Add your gameplay logic here, like dealing damage to the player or playing a sound
    }

    public bool IsStunned()
    {
        return isStunned;
    }

    public void ApplyStun(float duration)
    {
        isStunned = true;
        stunTimer = duration;
    }
}