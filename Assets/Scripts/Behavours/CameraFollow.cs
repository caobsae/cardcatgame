using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;            // Reference to the player's transform
    public Vector3 offset = new Vector3(0f, 0f, -10f);   // Camera offset from the player

    public float smoothSpeed = 0.125f;  // Smoothing factor for camera movement

    private void LateUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
    }
}
