using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    public Transform leftPoint;     // The left delimiting point
    public Transform rightPoint;    // The right delimiting point
    public Transform playerTransform;
    public Transform cardParent; // Add this line

    public List<GameObject> cardsToUse = new List<GameObject>();   // List of card prefabs
    public int numberOfCardsToRender = 5;                          // Number of cards to render

    private List<GameObject> renderedCards = new List<GameObject>();
    private List<Vector3> originalCardPositions = new List<Vector3>();

    public static CardManager Instance { get; private set; }

    public Card SelectedCard { get; private set; } // Property to store the selected card


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        ResizeAndPositionCards();
    }

    private void ResizeAndPositionCards()
    {
        if (cardsToUse.Count == 0 || numberOfCardsToRender == 0)
        {
            return;
        }

        float totalWidth = Vector3.Distance(leftPoint.position, rightPoint.position);
        float spacing = totalWidth / (numberOfCardsToRender + 1);

        float distance = Mathf.Abs(leftPoint.position.x - rightPoint.position.x);
        Debug.Log("Distance: " + distance);
        float resize = distance / numberOfCardsToRender;
        for (int i = 0; i < numberOfCardsToRender; i++)
        {
            float t = (i + 1) / (float)(numberOfCardsToRender + 1);
            Vector3 newPosition = Vector3.Lerp(leftPoint.position, rightPoint.position, t);
            newPosition.z = transform.position.z;

            GameObject cardPrefab = cardsToUse[i % cardsToUse.Count];
            GameObject newCard = Instantiate(cardPrefab, newPosition, Quaternion.identity);
            newCard.transform.localScale = new Vector3(resize, newCard.transform.localScale.y, newCard.transform.localScale.x);
            newCard.transform.parent = cardParent.transform;

            renderedCards.Add(newCard);
            originalCardPositions.Add(newCard.transform.position); // Keep track of original positions
            // Adjust any card-specific properties here
        }
    }

    public void SelectCard(int cardIndex)
    {
        for (int i = 0; i < renderedCards.Count; i++)
        {
            Vector3 originalPosition = renderedCards[i].transform.position;
            Vector3 newPosition = originalPosition;

            if (i == cardIndex)
            {
                newPosition += Vector3.up * 0.5f; // Slide the selected card up a bit
            }

            renderedCards[i].transform.position = newPosition;
        }
    }

    public void SelectCard(GameObject cardObject)
    {
        SelectedCard = cardObject.GetComponent<Card>();

        for (int i = 0; i < renderedCards.Count; i++)
        {
            Vector3 newPosition = originalCardPositions[i];
            if (renderedCards[i] == cardObject)
            {
                newPosition += Vector3.up * 0.5f;
            }
            renderedCards[i].transform.position = newPosition;
        }
    }

    public void ReturnCardsToOriginalPositions()
    {
        for (int i = 0; i < renderedCards.Count; i++)
        {
            renderedCards[i].transform.position = originalCardPositions[i];
        }
    }

}
