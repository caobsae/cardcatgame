using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardUI : MonoBehaviour
{
    public Card card; // Assign your card prefab to this field in the Inspector

    private void Start()
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(OnCardClicked);
    }

    private void OnCardClicked()
    {
        if (card != null)
        {
            card.Effect(); // Call the card's effect when the UI element is clicked
        }
    }
}
