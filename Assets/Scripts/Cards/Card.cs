using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum CardEffectType
{
    NONE,
    DPS,
    OneHit,
    InstaKill,
    OneEffect
}
public enum CardMagicEffect
{
    NONE,
    PASIVE,
    ACTIVE
}
public abstract class Card : MonoBehaviour
{

    public int munition;       // Number of uses/ammunition for the card
    public float cooldown;       // Cooldown time between uses
    public float cooldownTimer;
    public int Id;             // Identifier for the card
    public float damage; // The damage value of the card
    public Sprite spriteCat;
    public GameObject catObject;  // Reference to the card's sprite GameObject

    public TextMeshProUGUI munitionText;
    public TextMeshProUGUI cooldownText;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI descriptionText;


    public CardEffectType effectType; // The type of card effect
    public CardMagicEffect magicEffect;

    public BaseCat baseCat;

    private void Start()
    {
        nameText.text = baseCat.name;
        munitionText.text = munition.ToString();
        descriptionText.text = baseCat.description;
        spriteCat = baseCat.sprite;

        damage = baseCat.damage;
        cooldown = baseCat.cooldown;
        if (baseCat.effectType == CardMagicEffect.NONE)
        {
            effectType = baseCat.cardType;
        }
        else
        {
            magicEffect = baseCat.effectType;
        }
    }

    private void Update()
    {
        if (cooldownTimer > 0)
        {
            cooldownTimer -= Time.deltaTime;
        }
        UpdateText();
    }

    public bool CanUse()
    {
        return cooldownTimer <= 0f;
    }

    public abstract void Effect();

    private void OnMouseDown()
    {
        Debug.Log("tocado");
        CardManager.Instance.SelectCard(gameObject); // Calls the SelectCard function from CardManager
    }

    // Method to update the munition and cooldown text
    public void UpdateText()
    {
        try
        {
            munitionText.text = munition.ToString();
            cooldownText.text = cooldown.ToString();
        }
        catch
        {
            Debug.Log("No tiene el componente");
        }
    }
}
