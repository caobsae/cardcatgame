using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundClickHandler : MonoBehaviour
{
    private void OnMouseDown()
    {
        // Check if a card is selected
        if (CardManager.Instance.SelectedCard != null)
        {
            // Call the Effect method of the selected card
            CardManager.Instance.SelectedCard.Effect();
        }
    }
}
