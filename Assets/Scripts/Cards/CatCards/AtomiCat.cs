using UnityEngine;

public class AtomiCat : Card
{
    private void Awake()
    {
        munition = 1;
        cooldown = 180;
        Id = 3;
        effectType = CardEffectType.OneHit;
    }

    public override void Effect()
    {
        if (CanUse())
        {
            cooldownTimer = cooldown;
            munition--;

            DestroyAllEnemies();
        }
    }

    private void DestroyAllEnemies()
    {
        EnemyFollow[] enemies = FindObjectsOfType<EnemyFollow>();

        foreach (EnemyFollow enemy in enemies)
        {
            enemy.Die();
        }
    }
}