using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperCatSpin : Card
{
    public float spinDuration = 5f;
    public float spinRadius = 2f;
    public float spinSpeed = 90f;

    private GameObject spinningObject;
    private Vector3 initialPosition;

    private void Awake()
    {
        munition = 1;
        cooldown = 12;
        Id = 1;
        damage = 10;
        spinDuration = 7f;
        spinRadius = 3;
        spinSpeed = 200;
    }

    public override void Effect()
    {
        Debug.Log("Hoa");
        if (CanUse())
        {
            cooldownTimer = cooldown;
            munition--;

            Vector3 spawnPosition = CardManager.Instance.playerTransform.position + CardManager.Instance.playerTransform.forward * 2f;
            spinningObject = Instantiate(catObject, spawnPosition, Quaternion.identity);
            initialPosition = spinningObject.transform.position;

            StartCoroutine(SpinAroundPlayer());
        }
    }

    private IEnumerator SpinAroundPlayer()
    {
        float startTime = Time.time;

        while (Time.time - startTime < spinDuration)
        {
            float angle = (Time.time - startTime) * spinSpeed;
            float x = Mathf.Cos(angle * Mathf.Deg2Rad) * spinRadius;
            float y = Mathf.Sin(angle * Mathf.Deg2Rad) * spinRadius;

            Vector3 newPosition = CardManager.Instance.playerTransform.position + new Vector3(x, y, 0f);
            spinningObject.transform.position = newPosition;

            yield return null;
        }

        Destroy(spinningObject);
    }
}
