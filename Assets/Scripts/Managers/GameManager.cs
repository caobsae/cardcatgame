using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public List<EnemyFollow> activeEnemies = new List<EnemyFollow>();
    public List<Card> activeCards = new List<Card>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddActiveEnemy(EnemyFollow enemy)
    {
        activeEnemies.Add(enemy);
    }

    public void RemoveActiveEnemy(EnemyFollow enemy)
    {
        activeEnemies.Remove(enemy);
    }

    public void AddActiveCard(Card card)
    {
        activeCards.Add(card);
    }

    public void ReplaceActiveCard(Card oldCard, Card newCard)
    {
        int index = activeCards.IndexOf(oldCard);
        if (index != -1)
        {
            activeCards[index] = newCard;
        }
    }

    public void RemoveActiveCard(Card card)
    {
        activeCards.Remove(card);
    }
}