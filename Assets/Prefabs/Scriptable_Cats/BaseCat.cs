using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BaseCat : ScriptableObject
{
    public string name;
    public string description;
    public float damage;
    public float cooldown;
    public float liveTime;
    public Sprite sprite;
    public CardEffectType cardType;
    public CardMagicEffect effectType;
    public GameObject castCard;
}
